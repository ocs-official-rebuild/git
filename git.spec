%bcond_with svn

%global gitexecdir          %{_libexecdir}/git-core
%global bashcompdir         %(pkg-config --variable=completionsdir bash-completion 2>/dev/null)
%global bashcomproot        %(dirname %{bashcompdir} 2>/dev/null)

%global _package_note_file  %{_builddir}/%{name}-%{version}/.package_note-%{name}-%{version}-%{release}.%{_arch}.ld

Summary:        Fast Version Control System
Name:           git
Version:        2.41.1
Release:        2%{?dist}
License:        GPLv2
URL:            https://git-scm.com/
Source0:        https://github.com/%{name}/%{name}/archive/refs/tags/v%{version}.tar.gz
Source11:       git.xinetd.in
Source12:       git-gui.desktop
Source13:       gitweb-httpd.conf
Source14:       gitweb.conf.in
Source15:       git@.service.in
Source16:       git.socket

BuildRequires:  %{_bindir}/pod2man
BuildRequires:  docbook5-style-xsl, gawk, gettext
BuildRequires:  rubygem-asciidoctor, asciidoc
BuildRequires:  xmlto, coreutils, desktop-file-utils, diffutils, findutils, sed
BuildRequires:  expat-devel, libcurl-devel, openssl-devel, pcre2-devel
BuildRequires:  gcc, make, perl-generators, perl-interpreter, autoconf
BuildRequires:  perl-Error, perl(lib), perl(Test), perl(File::Compare)
BuildRequires:  pkgconfig(bash-completion), bash, highlight
BuildRequires:  systemd, tcl, tk, xz, zlib-devel >= 1.2, tar, time, zip
BuildRequires:  acl, glibc-all-langpacks, glibc-langpack-en, glibc-langpack-is
BuildRequires:  httpd, openssh-clients
BuildRequires:  perl(App::Prove), perl(DBD::SQLite), perl-MailTools
BuildRequires:  perl(CGI), perl(CGI::Carp), perl(CGI::Util)
BuildRequires:  perl(Digest::MD5), perl(Fcntl), perl(Memoize), perl(HTTP::Date)
BuildRequires:  perl(File::Basename), perl(File::Copy), perl(File::Find)
BuildRequires:  perl(filetest), perl(JSON), perl(JSON::PP), perl(POSIX)
BuildRequires:  perl(IO::Pty), perl(Term::ReadLine), perl(Test::More)
BuildRequires:  perl(Time::HiRes)
BuildRequires:  python3-devel
%if %{with svn}
BuildRequires:  subversion
BuildRequires:  subversion-perl
BuildRequires:  mod_dav_svn
%endif

Requires:       git-core = %{version}-%{release}
Requires:       git-core-doc = %{version}-%{release}
%if ! %{defined perl_bootstrap}
Requires:       perl(Term::ReadKey)
%endif
Requires:       perl-Git = %{version}-%{release}

%description
Git is a fast, scalable, distributed revision control system with an
unusually rich command set that provides both high-level operations
and full access to internals.

The git rpm installs common set of tools which are usually using with
small amount of dependencies. To install all git packages, including
tools for integrating with other SCMs, install the git-all meta-package.

%package all
Summary:        Meta-package to pull in all git tools
BuildArch:      noarch
Requires:       git = %{version}-%{release}
Requires:       git-credential-libsecret = %{version}-%{release}
Requires:       git-daemon = %{version}-%{release}
Requires:       git-email = %{version}-%{release}
Requires:       git-gui = %{version}-%{release}
Requires:       git-p4 = %{version}-%{release}
Requires:       git-subtree = %{version}-%{release}
%if %{with svn}
Requires:       git-svn = %{version}-%{release}
%endif
Requires:       git-instaweb = %{version}-%{release}
Requires:       gitk = %{version}-%{release}
Requires:       perl-Git = %{version}-%{release}
%if ! %{defined perl_bootstrap}
Requires:       perl(Term::ReadKey)
%endif

%description all
Git is a fast, scalable, distributed revision control system with an
unusually rich command set that provides both high-level operations
and full access to internals.

%package core
Summary:        Core package of git with minimal functionality
Requires:       less
Requires:       openssh-clients
Requires:       zlib >= 1.2

%description core
Git is a fast, scalable, distributed revision control system with an
unusually rich command set that provides both high-level operations
and full access to internals.

The git-core rpm installs really the core tools with minimal
dependencies. Install git package for common set of tools.
To install all git packages, including tools for integrating with
other SCMs, install the git-all meta-package.

%package core-doc
Summary:        Documentation files for git-core
BuildArch:      noarch
Requires:       git-core = %{version}-%{release}

%description core-doc
Documentation files for git-core package including man pages.

%package credential-libsecret
Summary:        Git helper for accessing credentials via libsecret
BuildRequires:  libsecret-devel
Requires:       git = %{version}-%{release}

%description credential-libsecret
%{summary}.

%package daemon
Summary:        Git protocol daemon
Requires:       git-core = %{version}-%{release}
Requires:       systemd
Requires(post): systemd
Requires(preun):  systemd
Requires(postun): systemd

%description daemon
The git daemon for supporting git:// access to git repositories

%package email
Summary:        Git tools for sending patches via email
BuildArch:      noarch
Requires:       git = %{version}-%{release}
Requires:       perl(Authen::SASL), perl(Email::Valid), perl(Cwd)
Requires:       perl(File::Spec), perl(File::Spec::Functions), perl(File::Temp)
Requires:       perl(IO::Socket::SSL), perl(Mail::Address)
Requires:       perl(MIME::Base64), perl(MIME::QuotedPrint)
Requires:       perl(Net::Domain), perl(Net::SMTP), perl(Net::SMTP::SSL)
Requires:       perl(POSIX), perl(Sys::Hostname), perl(Text::ParseWords)
Requires:       perl(Term::ANSIColor), perl(Term::ReadLine)

%description email
%{summary}.

%package -n gitk
Summary:        Git repository browser
BuildArch:      noarch
Requires:       git = %{version}-%{release}
Requires:       git-gui = %{version}-%{release}
Requires:       tk >= 8.4

%description -n gitk
%{summary}.

%package -n gitweb
Summary:        Simple web interface to git repositories
BuildArch:      noarch
Requires:       git = %{version}-%{release}

%description -n gitweb
%{summary}.

%package gui
Summary:        Graphical interface to Git
Requires:       gitk = %{version}-%{release}
Requires:       tk >= 8.4
BuildArch:      noarch

%description gui
%{summary}.

%package instaweb
Summary:        Repository browser in gitweb
Requires:       git = %{version}-%{release}
Requires:       gitweb = %{version}-%{release}
Requires:       httpd
BuildArch:      noarch

%description instaweb
A simple script to set up gitweb and a web server for browsing the local
repository.

%package p4
Summary:        Git tools for working with Perforce depots
BuildRequires:  python3-devel
Requires:       git = %{version}-%{release}
BuildArch:      noarch

%description p4
%{summary}.

%package -n perl-Git
Summary:        Perl interface to Git
Requires:       git = %{version}-%{release}
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
BuildArch:      noarch

%description -n perl-Git
%{summary}.

%if %{with svn}
%package -n perl-Git-SVN
Summary:        Perl interface to Git::SVN
Requires:       git = %{version}-%{release}
Requires:       perl-libs
BuildArch:      noarch

%description -n perl-Git-SVN
%{summary}.
%endif

%package subtree
Summary:        Git tools to merge and split repositories
Requires:       git-core = %{version}-%{release}

%description subtree
Git subtrees allow subprojects to be included within a subdirectory
of the main project, optionally including the subproject's entire
history.

%if %{with svn}
%package svn
Summary:        Git tools for interacting with Subversion repositories
Requires:       git = %{version}-%{release}
Requires:       perl(Digest::MD5)
%if ! %{defined perl_bootstrap}
Requires:       perl(Term::ReadKey)
%endif
Requires:       subversion
BuildArch:      noarch

%description svn
%{summary}.
%endif



%prep
%autosetup -p1 -n %{name}-%{version}


%{?perl_default_filter}
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}perl\\(packed-refs\\)
%if ! %{defined perl_bootstrap}
%global __requires_exclude %{?__requires_exclude:%__requires_exclude|}perl\\(Term::ReadKey\\)
%endif

cat << \EOF | tee config.mak
V = 1
CFLAGS = %{build_cflags}
LDFLAGS = %{build_ldflags}
USE_LIBPCRE = 1
ETC_GITCONFIG = %{_sysconfdir}/gitconfig
INSTALL_SYMLINKS = 1
GITWEB_PROJECTROOT = %{_localstatedir}/lib/git
GNU_ROFF = 1
NO_PERL_CPAN_FALLBACKS = 1
USE_ASCIIDOCTOR = 1
htmldir = %{_pkgdocdir}
prefix = %{_prefix}
	
DEFAULT_TEST_TARGET = prove
GIT_PROVE_OPTS = --verbose --normalize %{?_smp_mflags} --formatter=TAP::Formatter::File
GIT_TEST_OPTS = -x --verbose-log
EOF


rm -rf perl/Git/LoadCPAN{.pm,/}
rm -rf perl/FromCPAN/

grep -rlZ '^use Git::LoadCPAN::' | xargs -r0 sed -i 's/Git::LoadCPAN:://g'


%build
make configure
./configure --prefix=/usr

%make_build all doc
%make_build -C contrib/contacts/ all
%make_build -C contrib/credential/libsecret/
%make_build -C contrib/credential/netrc/
%make_build -C contrib/diff-highlight/
%make_build -C contrib/subtree/ all

# fix shebang build error
sed -i -e '1s@#!\( */usr/bin/env python\|%{__python2}\)$@#!%{__python3}@' git-p4.py

%install
%make_install install-doc
%make_install -C contrib/contacts

install -pm 755 contrib/credential/libsecret/git-credential-libsecret \
    %{buildroot}%{gitexecdir}
install -pm 755 contrib/credential/netrc/git-credential-netrc \
    %{buildroot}%{gitexecdir}
mv contrib/credential/netrc .

%make_install -C contrib/subtree

mkdir -p %{buildroot}%{_sysconfdir}/httpd/conf.d
install -pm0644 %{SOURCE13} %{buildroot}%{_sysconfdir}/httpd/conf.d/gitweb.conf
sed "s|@PROJECTROOT@|%{_localstatedir}/lib/git|g" \
    %{SOURCE14} > %{buildroot}%{_sysconfdir}/gitweb.conf

install -D -pm755 contrib/diff-highlight/diff-highlight \
    %{buildroot}%{_datadir}/git-core/contrib/diff-highlight
rm -rf contrib/diff-highlight/{Makefile,diff-highlight,*.perl,t}

rm -rf contrib/scalar
rm -rf contrib/subtree/{INSTALL,Makefile,git-subtree*,t}
rm -f %{buildroot}%{gitexecdir}/git-remote-testsvn

mkdir -p %{buildroot}%{_localstatedir}/lib/git
install -D -pm0644 %{SOURCE16} %{buildroot}%{_unitdir}/git.socket
perl -p \
    -e "s|\@GITEXECDIR\@|%{gitexecdir}|g;" \
    -e "s|\@BASE_PATH\@|%{_localstatedir}/lib/git|g;" \
    %{SOURCE15} > %{buildroot}%{_unitdir}/git@.service

install -Dpm644 contrib/completion/git-completion.bash %{buildroot}%{bashcompdir}/git
ln -s git %{buildroot}%{bashcompdir}/gitk

mkdir -p %{buildroot}%{_datadir}/git-core/contrib/completion
install -pm644 contrib/completion/git-completion.tcsh \
    %{buildroot}%{_datadir}/git-core/contrib/completion/

mkdir -p %{buildroot}%{_datadir}/git-core/contrib
mv contrib/hooks %{buildroot}%{_datadir}/git-core/contrib
pushd contrib
ln -s ../../../git-core/contrib/hooks
popd

mkdir -p %{buildroot}%{_datadir}/git-core/contrib/completion
install -pm644 contrib/completion/git-prompt.sh \
    %{buildroot}%{_datadir}/git-core/contrib/completion/

desktop-file-install --dir=%{buildroot}%{_datadir}/applications %{SOURCE12}

pushd %{buildroot}%{gitexecdir} >/dev/null
if cmp -s git-gui git-citool 2>/dev/null; then
    ln -svf git-gui git-citool
fi
popd >/dev/null

%find_lang %{name} %{name}.lang

chmod -R g-w %{buildroot}
chmod a-x %{buildroot}%{gitexecdir}/git-mergetool--lib
chmod a-x Documentation/technical/api-index.sh
find contrib -type f -print0 | xargs -r0 chmod -x

rm -rf %{buildroot}%{gitexecdir}/git-svn
rm -rf %{buildroot}%{_mandir}/man1/git-svn.1*
%if %{without svn}
rm -rf %{buildroot}%{_datadir}/perl5/Git/SVN*
%endif


%check
GIT_SKIP_TESTS="t5559"

%ifarch aarch64 %{power64}
GIT_SKIP_TESTS="$GIT_SKIP_TESTS t5541.35 t5551.25"
%endif

%ifarch %{power64}
GIT_SKIP_TESTS="$GIT_SKIP_TESTS t9115"
%endif

export GIT_SKIP_TESTS
export LANG=en_US.UTF-8
export GIT_TEST_GIT_DAEMON=true
export GIT_TEST_HTTPD=false
%if %{with svn}
export GIT_TEST_SVNSERVE=true
export GIT_TEST_SVN_HTTPD=true
%else
export GIT_TEST_SVNSERVE=false
export GIT_TEST_SVN_HTTPD=false
%endif


%__make -C t all

mkdir -p contrib/credential
mv netrc contrib/credential/
%make_build -C contrib/credential/netrc/ test || \
%make_build -C contrib/credential/netrc/ testverbose


%post daemon
%systemd_post git.socket

%preun daemon
%systemd_preun git.socket

%postun daemon
%systemd_postun_with_restart git.socket

%files
%{_datadir}/git-core/contrib/diff-highlight
%{_datadir}/git-core/templates/hooks/fsmonitor-watchman.sample
%{_datadir}/git-core/templates/hooks/pre-rebase.sample
%{_datadir}/git-core/templates/hooks/prepare-commit-msg.sample

%files all

%files core
%license COPYING
%{_bindir}/git
%{_bindir}/git-cvsserver
%{_bindir}/git-receive-pack
%{_bindir}/git-shell
%{_bindir}/git-upload-archive
%{_bindir}/git-upload-pack
%{_bindir}/scalar
%{_datadir}/git-core/
%{_datadir}/locale/*
%{gitexecdir}/*
%exclude %{_datadir}/git-core/contrib/diff-highlight
%exclude %{_datadir}/git-core/contrib/hooks/update-paranoid
%exclude %{_datadir}/git-core/contrib/hooks/setgitperms.perl
%exclude %{_datadir}/git-core/templates/hooks/fsmonitor-watchman.sample
%exclude %{_datadir}/git-core/templates/hooks/pre-rebase.sample
%exclude %{_datadir}/git-core/templates/hooks/prepare-commit-msg.sample
%{bashcomproot}

%files core-doc -f %{name}.lang
%{_mandir}/man*/git*
%{_mandir}/man1/scalar.1.gz
%exclude %{_mandir}/man1/git-daemon*.1*
%exclude %{_mandir}/man1/*email*.1*
%exclude %{_mandir}/man1/*gitk*.1*
%exclude %{_mandir}/man1/gitweb.1*
%exclude %{_mandir}/man5/gitweb.conf.5*
%exclude %{_mandir}/man1/git-gui.1*
%exclude %{_mandir}/man1/git-citool.1*
%exclude %{_mandir}/man1/git-instaweb.1*
%exclude %{_mandir}/man1/*p4*.1*
%exclude %{_mandir}/man3/Git.3pm*
%if %{with svn}
%exclude %{_mandir}/man1/git-svn.1*
%endif


%files credential-libsecret
%{gitexecdir}/git-credential-libsecret

%files daemon
%doc Documentation/git-daemon*.txt Documentation/git-daemon*.html
%{_unitdir}/git.socket
%{_unitdir}/git@.service
%{gitexecdir}/git-daemon
%{_localstatedir}/lib/git
%{_mandir}/man1/git-daemon*.1*

%files email
%doc Documentation/{*email*.txt,*email*.html}
%{gitexecdir}/*email*
%{_mandir}/man1/*email*.1*

%files -n gitk
%doc Documentation/{*gitk*.txt,*gitk*.html}
%{_bindir}/*gitk*
%{_datadir}/gitk
%{_mandir}/man1/*gitk*.1*

%files -n gitweb
%{_datadir}/gitweb/gitweb.cgi
%{_datadir}/gitweb/static/git-favicon.png
%{_datadir}/gitweb/static/git-logo.png
%{_datadir}/gitweb/static/gitweb.css
%{_datadir}/gitweb/static/gitweb.js
%config(noreplace)%{_sysconfdir}/gitweb.conf
%config(noreplace)%{_sysconfdir}/httpd/conf.d/gitweb.conf
%{_mandir}/man1/gitweb.1*
%{_mandir}/man5/gitweb.conf.5*

%files gui
%{gitexecdir}/git-gui*
%{gitexecdir}/git-citool
%{_datadir}/applications/*git-gui.desktop
%{_datadir}/git-gui/
%{_mandir}/man1/git-gui.1*
%{_mandir}/man1/git-citool.1*

%files instaweb
%{gitexecdir}/git-instaweb
%{_mandir}/man1/git-instaweb.1*

%files p4
%{gitexecdir}/*p4*
%{gitexecdir}/mergetools/p4merge
%{_mandir}/man1/*p4*.1*

%files -n perl-Git
%{_datadir}/perl5/Git.pm
%{_datadir}/perl5/Git/I18N.pm
%{_datadir}/perl5/Git/IndexInfo.pm
%{_datadir}/perl5/Git/Packet.pm
%{_mandir}/man3/Git.3pm*

%if %{with svn}
%files -n perl-Git-SVN
%{_datadir}/perl5/Git/SVN.pm
%{_datadir}/perl5/Git/SVN/Editor.pm
%{_datadir}/perl5/Git/SVN/Fetcher.pm
%{_datadir}/perl5/Git/SVN/GlobSpec.pm
%{_datadir}/perl5/Git/SVN/Log.pm
%{_datadir}/perl5/Git/SVN/Memoize/YAML.pm
%{_datadir}/perl5/Git/SVN/Migration.pm
%{_datadir}/perl5/Git/SVN/Prompt.pm
%{_datadir}/perl5/Git/SVN/Ra.pm
%{_datadir}/perl5/Git/SVN/Utils.pm
%endif

%files subtree
%{gitexecdir}/git-subtree

%if %{with svn}
%files svn
%{gitexecdir}/git-svn
%{_mandir}/man1/git-svn.1*
%endif

%changelog
* Mon Jun 24 2024 Rebuild Robot <rebot@opencloudos.org> - 2.41.1-2
- [Type] security
- [DESC] Rebuilt for less

* Mon May 20 2024 Upgrade Robot <upbot@opencloudos.tech> - 2.41.1-1
- Upgrade to version 2.41.1
- fix CVE-2024-32002, CVE-2024-32004, CVE-2024-32465

* Mon Oct 23 2023 cunshunxia <cunshunxia@tencent.com> - 2.41.0-10
- fix git config path to /etc/ instead of /usr/etc/.

* Thu Oct 12 2023 Miaojun Dong <zoedong@tencent.com> - 2.41.0-9
- Rebuild for curl-8.4.0

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.41.0-8
- Rebuilt for OpenCloudOS Stream 23.09

* Thu Aug 03 2023 cunshunxia <cunshunxia@tencent.com> - 2.41.0-7
- Rebuilt for tcl 8.6.13

* Wed Jul 19 2023 cunshunxia <cunshunxia@tencent.com> - 2.41.0-6
- Rebuilt for httpd 2.4.57

* Tue Jun 13 2023 cunshunxia <cunshunxia@tencent.com> - 2.37.3-5
- make git-core no more require perl(Git::LoadCPANXXX).

* Wed Jun 7 2023 cunshunxia <cunshunxia@tencent.com> - 2.37.3-4
- remove perl(Error) in perl-Git which is provided by perl-Error already.

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.37.3-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.37.3-2
- Rebuilt for OpenCloudOS Stream 23

* Wed Sep 21 2022 cunshunxia <cunshunxia@tencent.com> - 2.37.3-1
- initial build
